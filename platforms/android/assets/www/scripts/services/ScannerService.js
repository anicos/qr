'use strict';



angular.module('yoApp')
.service('ScannerService',['$q', 'CordovaReady',function ($q, CordovaReady) {

	this.scan = function(scope){
		var deferred = $q.defer();
		
		
		 CordovaReady().then(function(scope){
			 alert("1");
			 var scanner = cordova.plugins.barcodeScanner;
		       scanner.scan(
				        function(result) {
				        	if(scope){
								scope.$apply(function(){ //(4)
									deferred.resolve(result); //(5)
								});
							}else{
								deferred.resolve(result); //(5)
							}
				    }, function(error) {
				    	deferred.reject("Scan failed: " + error);
				    });
		 
		    
		 });
		 
		 return deferred.promise;
	}
	
	
}]);