'use strict';

angular.module('yoApp')
  .controller('MainCtrl', ['$scope','ScannerService', function ($scope, ScannerService) {
	  var scannerService =ScannerService;
	    
   $scope.onButton2Click = function() {
	   scannerService.scan($scope).then(function(result) {
		   alert("Scanned Code: " + result.text 
	                + ". Format: " + result.format
	                + ". Cancelled: " + result.cancelled);
	});
   };
   
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  }]);
